// Copy this file to an reasonable name for the event, no spaces or special
// characters.
// Make sure the new filename ends in '.adoc'.
// All attributes need to be declared or unset.
// The exclamation mark at the end unsets them.
// The unset attributes need to retain that exclamation mark.
// Fill out at least event-name, event-dates, event-location and
// event-updatestatus.
// Supply any other attributes that are available, remove the eclamation mark
// to activate them.
:event-name: APconf
:event-tag: #APconf
:event-dates: October 2 - 5
:event-site!:
:event-location: online
:event-updateurl!:
:event-updatestatus: CfP opening soon
:event-updatenotes: planning starting now, join #APconf on Freenode to help the Activity Pub conference
